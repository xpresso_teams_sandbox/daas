"""
This is a sample hello world app
It prints hello world 100 times
"""
__author__ = "Naveen Sinha"

import pandas  as pd
import numpy as np
from datetime import datetime


def read_csv_link(link):
  df = pd.read_csv(link)
  return df


def gold_month_end():
  b = datetime.today().strftime('%Y%m%d')
  gold_price_link_monthly_stooq = 'https://stooq.com/q/d/l/?s=xauusd&d1=19900101&d2={}&i=m'.format(b)
  df = read_csv_link(gold_price_link_monthly_stooq)
  df['Date'] = pd.to_datetime(df['Date'])
  df = df.loc[df['Date'] >= '1990-01-01']
  df['Date'] = df['Date'].dt.strftime('%m-%d-%Y')
  df = df.rename(columns={"Date": "AsOfDate", "Close": "GoldPrice"})
  df["AsOfDate"] = df['AsOfDate'].astype('object')
  df = df[["AsOfDate", "GoldPrice"]]
  return df


def test_version():
  try:
    df = gold_month_end()
    df.to_csv('/data/gold_price.csv', index=False, encoding='utf-8-sig')
  except Exception as e:
    print(e)


if __name__ == '__main__':
  test_version()

